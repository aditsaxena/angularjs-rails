FactoryGirl.define do
  factory :post do
    sequence :title do |n| "The Post Title #{n}" end
    desc { "MyText #{rand}" }
    like [true, false].sample
  end
end


