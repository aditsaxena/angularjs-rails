app = angular.module 'app', []

app.factory 'Post', ($http) ->
  Post =
    get: ->
      $http.get("/posts.json").success((data, status, headers, config) ->
        Post.posts = data
        return
      ).error (data, status, headers, config) ->
        # log error

@PostCtrl = ($scope, $http, Post) ->
  Post.get()
  $scope.posts = Post
