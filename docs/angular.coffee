# Organize large apps
#   - http://cliffmeyers.com/blog/2013/4/21/code-organization-angularjs-javascript

myApp.filter 'reverse', (Post) ->
  (message) ->
    message.split("").reverse().join("") + " # " + Post.message

# Usage: <mywidget>
app.directive 'mywidget', ->
  restrict: 'E'
  template: '<div style="padding:5px; background:#eee; display:inline-block;">This is the content</div>'

# Usage: <div mywidget></div>
app.directive 'mywidget', ->
  restrict: 'A'
  template: '<div style="padding:5px; background:#eee; display:inline-block;">This is the content</div>'

# Usage: <!-- directive:mywidget -->
app.directive 'mywidget', ->
  restrict: 'M'
  template: '<div style="padding:5px; background:#eee; display:inline-block;">This is the content</div>'

# Shortcut for attribute:
#   %div{ style: "background: #ccc", "myhover" => "40px" }
app.directive 'myhover', ->
  (scope, element, attrs) ->
    element.bind 'mouseenter', ->
      element.css 'padding', attrs.myhover
    element.bind 'mouseleave', ->
      element.css 'padding', 'inherit'

# Directive talking to another directive
#   %hoverable{ style: "background: #ccc", "funny" => "hilarious", "debuggable" => "" } mywidget
app.directive 'hoverable', ->
  restrict: 'E'
  scope: {}
  controller: ($scope) ->
    $scope.my_attributes = []
    @scope = $scope
    @add_fun = (level) -> $scope.my_attributes.push("funAdded#{level}")
    @
  
  link: (scope, element, attrs) ->

app.directive 'funny', ->
  require: 'hoverable'
  link: (scope, element, attrs, widgetCtrl) ->
    element.bind 'mouseenter', -> widgetCtrl.add_fun(attrs.funny)

app.directive 'debuggable', ->
  require: 'hoverable'
  link: (scope, element, attrs, widgetCtrl) ->
    element.bind 'mouseenter', -> console.log widgetCtrl.scope.my_attributes


# Directive talking to controller & scope
#   %div{ "ng-controller" => "BookCtrl" }
#     %div{ "wewanna" => "", "mything" => "{{listmax}}" }
app.controller 'BookCtrl', ($scope) ->
  $scope.listmax = 5

app.directive 'wewanna', ->
  scope:
    mything: "@"
  template: "<div>max: {{mything}}</div>" # max: 5


# Bind scope directive-controller
#   %div{ "ng-controller" => "BookCtrl" }
#     %button{ "ng-click" => "debugme('ciao')" }normal
#     %div{ "thedirective" => "", "callbacker" => "debugme(msg)" }
app.controller 'BookCtrl', ($scope) ->
  $scope.debugme = (msg) ->
    console.log msg, 'debugd'

app.directive 'thedirective', ->
  scope:
    callbacker: "&"
  template: """<input type="text" ng-model="themsg">
      <button ng-click="callbacker({msg: themsg})">clk: {{themsg}}</button>"""


# Scopes
#   %div{ "ng-controller"=>"AppCtrl"}
#     %phone{ number: "555-1234", network: "network", :"make-call"=>"leaveVoicemail(number, message)" }
#     %phone{ number: "867-5309", network: "network", :"make-call"=>"leaveVoicemail(number, message)" }
#     %phone{ number: "911", network: "network", :"make-call"=>"leaveVoicemail(number, message)" }
app.controller "AppCtrl", ($scope) ->
  $scope.leaveVoicemail = (number, message) ->
    console.log "Number: #{number} said: #{message}"
    return
  return

app.directive "phone", ->
  restrict: "E"
  scope:
    number: "@"
    network: "="
    makeCall: "&"

  template: """
    <div class="panel">
      Number: {{number}} Network:
      <select ng-model="network" ng-options="net for net in networks">
      <input type="text" ng-model="value">
      <button ng-click="makeCall({number: number, message: value})">Call home!</button>
    </div>"""
  link: (scope) ->
    scope.networks = ["Verizon", "AT&T", "Sprint"]
    scope.network = scope.networks[0]
    return


